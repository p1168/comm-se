package com.pgf.alogic;

import java.util.Arrays;

/**
 * 插入排序
 * 逻辑:
 * 将一个数据 插入到拍好的数据列表中
 * 例如: 8,7,9,3,2
 * 先将8 7排序，将9插入到合适的位置，再将3插入到合适的位置这样依次成为
 * 7，8
 * 7，8，9
 * 3，7，8，9
 * 2，3，7，8，9
 */
public class InsertSort {

    public static int[] sort(int []arr,String flag){
        System.out.println("原数据:"+Arrays.toString(arr));
        if("desc".equals(flag)){
            System.out.println("逆序:");
        }else{
            System.out.println("升序:");
        }

        for(int i=0;i<arr.length-1;i++){
            for(int j=i+1;j>0;j--){
                System.out.println(arr[j]+"==="+arr[i]);
                if("desc".equals(flag)){
                    if(arr[j]>arr[j-1]){
                        int tmp = arr[j-1];
                        arr[j-1] = arr[j];
                        arr[j] = tmp;
                    }else{
                        break;
                    }
                }else{
                    if(arr[j]<arr[j-1]){
                        int tmp = arr[j-1];
                        arr[j-1] = arr[j];
                        arr[j] = tmp;
                    }else{
                        break;
                    }
                }

            }
        }
        return arr;
    }

    public static void main(String[] args) {
        int [] arr = {11,3,5,9,6,23,45,32,15,8,9};
        int [] arr2 = InsertSort.sort(arr,"desc");
        System.out.println(Arrays.toString(arr));
    }


}
