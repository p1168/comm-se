package com.pgf.alogic;

import java.util.Arrays;

/**
 * 冒泡排序
 */
public class SortBubble {

    /**
     * 将第一个数据和第二个比较，得出最大与第三个....第一轮得到最大放在最后
     *
     * @param arr 数据
     * @param flag 升序、倒序
     */
    public static int[] sort(int []arr,String flag){
        System.out.println("原数据:"+Arrays.toString(arr));
        if("desc".equals(flag)){
            System.out.println("逆序:");
        }else{
            System.out.println("升序:");
        }
        for (int i=0;i<arr.length-i;i++){
            for(int j=0;j<arr.length-i-1;j++){
                if("asc".equals(flag)){
                    if(arr[j]>arr[j+1]){
                        int tmp = arr[j];
                        arr[j] = arr[j+1];

                        arr[j+1] = tmp;
                    }
                }else{
                    if(arr[j]<arr[j+1]){
                        int tmp = arr[j];
                        arr[j] = arr[j+1];
                        arr[j+1] = tmp;
                    }
                }
            }
        }
        return arr;
    }

    public static void main(String[] args) {
        int [] arr = {11,3,5,9,6,23,45,32,15,8,9};
        int [] arr2 = SortBubble.sort(arr,"desc");
        System.out.println(Arrays.toString(arr));
    }

}
