package com.pgf.alogic;

import java.util.Arrays;

/**
 * 选择排序
 * 1、选出最大的，和第一个交换位置
 * 2、选出第二，和第二个交换位置....(有序区、无序区选值)
 */
public class SelectSort {

    /**
     * 将第一个数据和第二个比较，得出最大与第三个....第一轮得到最大放在最后
     *
     * @param arr 数据
     * @param flag 升序、倒序
     */
    public static int[] sort(int []arr,String flag){
        System.out.println("原数据:"+Arrays.toString(arr));
        if("desc".equals(flag)){
            System.out.println("逆序:");
        }else{
            System.out.println("升序:");
        }
        for (int i=0;i<arr.length;i++){
            int minP = i;
           for(int j=i;j<arr.length;j++){
               if("desc".equals(flag)){
                   if(arr[j]>arr[minP]){
                       minP = j;
                   }
               }else{
                   if(arr[j]<arr[minP]){
                       minP = j;
                   }
               }

           }
           int tmp = arr[i];
           arr[i] = arr[minP];
           arr[minP] = tmp;
        }
        return arr;
    }

    public static void main(String[] args) {
        int [] arr = {11,3,5,9,6,23,45,32,15,8,9,12};
        int [] arr2 = SelectSort.sort(arr,"asc");
        System.out.println(Arrays.toString(arr));
    }


}
