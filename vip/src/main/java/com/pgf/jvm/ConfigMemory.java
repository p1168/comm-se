package com.pgf.jvm;

import javax.sound.midi.Soundbank;
import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ConfigMemory {

    // -Xms20m -Xmx20m -Xmn10m -XX:+PrintGCDetails
    //设置大对象进入老年代的阈值
    public static final int ONE_M = 2*1024*1024;
    public static void main(String[] args) {
         Studnet pgf = new Studnet("pgf", 28);
        /*WeakReference<Studnet> ref = new WeakReference<>(pgf);
        pgf = null;//保证内存只有软引用的student
        System.out.println("before gc:"+ref.get());
        System.gc();
        System.out.println("after gc:"+ref.get());*/
        change(pgf);
        System.out.println(pgf);
        String aa = "123";

        /*String [] aa1 = {"1","2","3"};
        change(aa1);
        System.out.println(aa1);*/
        System.out.println("----------");
    }

    public static void  change(Studnet s){
        s.setName("132");
        //System.out.println(s);
    }

    /**
     * 堆分为 新生代(eden、s0、s1) 老年代
     * new 的对象一般搜先会在den去分配,当eden空间不够，
     */
}

class Studnet{
    private String name;
    private int age;

    public void setName(String name) {
        this.name = name;
    }

    public Studnet(String name, int age){
        this.age = age;
        this.name = name;
    }

    @Override
    public String toString() {
        return "Studnet{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
